#include <iostream>
#include "fonction.hpp"

int main(){
    std::cout<<"ok le main est lance"<<"\n";
    fonction fonct1;
    fonction fonct2(5, 9);
    std::cout<<"somme fonct1: "<< fonct1.somme()<<"\n";
    std::cout<<"somme fonct2: "<< fonct2.somme()<<"\n";
    std::cout<<"dans main"<<"\n";
}